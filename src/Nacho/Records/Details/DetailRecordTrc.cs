using System;
using Nacho.Extensions;

namespace Nacho.Records
{
    public class DetailRecordTrc : AchRecord
    {
        public override string TypeName => "TRC Detail";

        public string TransactionCode
        {
            get => RecordData.GetString(1, 2);
            set => RecordData.SetString(1, 2, value);
        }

        public string ReceivingDfiIdentification
        {
            get => RecordData.GetString(3, 8);
            set => RecordData.SetString(3, 8, value);
        }

        public char CheckDigit
        {
            get => RecordData.GetChar(11);
            set => RecordData.SetChar(11, value);
        }

        public string ReceivingRoutingNumber
        {
            get => RecordData.GetString(3, 9);
            set => RecordData.SetString(3, 9, value);
        }

        public string DfiAccountNumber
        {
            get => RecordData.GetString(12, 17);
            set => RecordData.SetString(12, 17, value);
        }

        public long? Amount
        {
            get => RecordData.GetInt64(29, 10);
            set => RecordData.SetInt64(29, 10, value);
        }

        public string CheckSerialNumber
        {
            get => RecordData.GetString(39, 15);
            set => RecordData.SetString(39, 15, value);
        }

        public string ProcessControlField
        {
            get => RecordData.GetString(54, 6);
            set => RecordData.SetString(54, 6, value);
        }

        public string ItemResearchNumber
        {
            get => RecordData.GetString(60, 16);
            set => RecordData.SetString(60, 16, value);
        }

        public string ItemTypeIndicator
        {
            get => RecordData.GetString(76, 2);
            set => RecordData.SetString(76, 2, value);
        }

        public char AddendaRecordIndicator
        {
            get => RecordData.GetChar(78);
            set => RecordData.SetChar(78, value);
        }

        public string TraceNumber
        {
            get => RecordData.GetString(79, 15);
            set => RecordData.SetString(79, 15, value, '0');
        }

        public DetailRecordTrc(ReadOnlySpan<char> record)
            : base(record)
        { }

        public DetailRecordTrc(ReadOnlyMemory<char> record)
            : base(record)
        { }
    }
}
