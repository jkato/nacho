using System;
using Nacho.Extensions;

namespace Nacho.Records
{
    public class FileControlRecord : AchRecord
    {
        public override string TypeName => "File Control";

        public int BatchCount
        {
            get => RecordData.GetInt32(1, 6) ?? 0;
            set => RecordData.SetInt32(1, 6, value);
        }

        public int BlockCount
        {
            get => RecordData.GetInt32(7, 6) ?? 0;
            set => RecordData.SetInt32(7, 6, value);
        }

        public int EntryAddendaCount
        {
            get => RecordData.GetInt32(13, 8) ?? 0;
            set => RecordData.SetInt32(13, 8, value);
        }

        public string EntryHash
        {
            get => RecordData.GetString(21, 10);
            set => RecordData.SetString(21, 10, value);
        }

        public long TotalDebitAmount
        {
            get => RecordData.GetInt64(31, 12) ?? 0;
            set => RecordData.SetInt64(31, 12, value);
        }

        public long TotalCreditAmount
        {
            get => RecordData.GetInt64(43, 12) ?? 0;
            set => RecordData.SetInt64(43, 12, value);
        }

        public string Reserved
        {
            get => RecordData.GetString(55, 39);
            set => RecordData.SetString(55, 39, value);
        }

        public FileControlRecord(ReadOnlySpan<char> record)
            : base(record)
        { }

        public FileControlRecord(ReadOnlyMemory<char> record)
            : base(record)
        { }
    }
}
