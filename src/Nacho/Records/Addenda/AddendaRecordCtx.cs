using System;

namespace Nacho.Records
{
    public class AddendaRecordCtx : AddendaRecordType5
    {
        public override string TypeName => "CTX Addenda";

        public AddendaRecordCtx(ReadOnlySpan<char> record)
            : base(record)
        { }

        public AddendaRecordCtx(ReadOnlyMemory<char> record)
            : base(record)
        { }
    }
}
