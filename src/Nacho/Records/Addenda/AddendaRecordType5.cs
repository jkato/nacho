using System;
using Nacho.Extensions;


namespace Nacho.Records
{
    public class AddendaRecordType5 : AchRecord
    {
        public string AddendaTypeCode
        {
            get => RecordData.GetString(1, 2);
            set => RecordData.SetString(1, 2, value);
        }

        public string PaymentRelatedInformation
        {
            get => RecordData.GetString(3, 80);
            set => RecordData.SetString(3, 80, value);
        }

        public int? AddendaSequenceNumber
        {
            get => RecordData.GetInt32(83, 4);
            set => RecordData.SetInt32(83, 4, value);
        }

        public int? EntryDetailSequenceNumber
        {
            get => RecordData.GetInt32(87, 7);
            set => RecordData.SetInt32(87, 7, value);
        }

        public AddendaRecordType5(ReadOnlySpan<char> record)
            : base(record)
        { }

        public AddendaRecordType5(ReadOnlyMemory<char> record)
            : base(record)
        { }
    }
}
