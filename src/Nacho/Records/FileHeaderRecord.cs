using System;
using Nacho.Extensions;

namespace Nacho.Records
{
    public class FileHeaderRecord : AchRecord
    {
        public override string TypeName => "File Header";

        public int? PriorityCode
        {
            get => RecordData.GetInt32(1, 2);
            set => RecordData.SetInt32(1, 2, value);
        }

        public string ImmediateDestination
        {
            get => RecordData.GetString(3, 10);
            set => RecordData.SetString(3, 10, value?.ToUpper());
        }

        public string ImmediateOrigin
        {
            get => RecordData.GetString(13, 10);
            set => RecordData.SetString(13, 10, value);
        }

        public DateTime? FileCreationDateTime
        {
            get => RecordData.GetDateTime(23, "yyMMddHHmm");
            set => RecordData.SetDateTime(23, "yyMMddHHmm", value);
        }

        public char FileIdModifier
        {
            get => RecordData.GetChar(33);
            set => RecordData.SetChar(33, value);
        }

        public int? RecordSize
        {
            get => RecordData.GetInt32(34, 3);
            set => RecordData.SetInt32(34, 3, value);
        }

        public int? BlockingFactor
        {
            get => RecordData.GetInt32(37, 2);
            set => RecordData.SetInt32(37, 2, value);
        }

        public char FormatCode
        {
            get => RecordData.GetChar(39);
            set => RecordData.SetChar(39, value);
        }

        public string ImmediateDestinationName
        {
            get => RecordData.GetString(40, 23);
            set => RecordData.SetString(40, 23, value);
        }

        public string ImmediateOriginName
        {
            get => RecordData.GetString(63, 23);
            set => RecordData.SetString(63, 23, value);
        }

        public string ReferenceCode
        {
            get => RecordData.GetString(86, 8);
            set => RecordData.SetString(86, 8, value);
        }

        public FileHeaderRecord(ReadOnlySpan<char> record)
            : base(record)
        { }

        public FileHeaderRecord(ReadOnlyMemory<char> record)
            : base(record)
        { }
    }
}
