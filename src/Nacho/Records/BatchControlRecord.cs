using System;
using Nacho.Extensions;

namespace Nacho.Records
{
    public class BatchControlRecord : AchRecord
    {
        public override string TypeName => "Batch Control";

        public string ServiceClassCode
        {
            get => RecordData.GetString(1, 3);
            set => RecordData.SetString(1, 3, value, '0');
        }

        public int EntryAddendaCount
        {
            get => RecordData.GetInt32(4, 6) ?? 0;
            set => RecordData.SetInt32(4, 6, value);
        }

        public string EntryHash
        {
            get => RecordData.GetString(10, 10);
            set => RecordData.SetString(10, 10, value, '0');
        }

        public long TotalDebitAmount
        {
            get => RecordData.GetInt64(20, 12) ?? 0;
            set => RecordData.SetInt64(20, 12, value);
        }

        public long TotalCreditAmount
        {
            get => RecordData.GetInt64(32, 12) ?? 0;
            set => RecordData.SetInt64(32, 12, value);
        }

        public string CompanyIdentification
        {
            get => RecordData.GetString(44, 10);
            set => RecordData.SetString(44, 10, value);
        }

        public string MessageAuthenticationCode
        {
            get => RecordData.GetString(54, 19);
            set => RecordData.SetString(54, 19, value);
        }

        public string Reserved
        {
            get => RecordData.GetString(73, 6);
            set => RecordData.SetString(73, 6, value);
        }

        public string OriginatingDfiIdentification
        {
            get => RecordData.GetString(79, 8);
            set => RecordData.SetString(79, 8, value);
        }

        public int BatchNumber
        {
            get => RecordData.GetInt32(87, 7) ?? 0;
            set => RecordData.SetInt32(87, 7, value);
        }

        public BatchControlRecord(ReadOnlySpan<char> record)
            : base(record)
        { }

        public BatchControlRecord(ReadOnlyMemory<char> record)
            : base(record)
        { }
    }
}
