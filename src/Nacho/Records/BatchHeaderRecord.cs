using System;
using Nacho.Extensions;

namespace Nacho.Records
{
    public class BatchHeaderRecord : AchRecord
    {
        public override string TypeName => "Batch Header";

        public string ServiceClassCode
        {
            get => RecordData.GetString(1, 3);
            set => RecordData.SetString(1, 3, value, '0');
        }

        public string CompanyName
        {
            get => RecordData.GetString(4, 16);
            set => RecordData.SetString(4, 16, value);
        }

        public string CompanyDiscretionaryData
        {
            get => RecordData.GetString(20, 20);
            set => RecordData.SetString(20, 20, value);
        }

        public string CompanyIdentification
        {
            get => RecordData.GetString(40, 10);
            set => RecordData.SetString(40, 10, value);
        }

        public string EntryClassCode
        {
            get => RecordData.GetString(50, 3);
            set => RecordData.SetString(50, 3, value);
        }

        public string CompanyEntryDescription
        {
            get => RecordData.GetString(53, 10);
            set => RecordData.SetString(53, 10, value);
        }

        public string CompanyDescriptiveDate
        {
            get => RecordData.GetString(63, 6);
            set => RecordData.SetString(63, 6, value);
        }

        public DateTime? EffectiveEntryDate
        {
            get => RecordData.GetDateTime(69, "yyMMdd");
            set => RecordData.SetDateTime(69, "yyMMdd", value);
        }

        public int? SettlementDate
        {
            get => RecordData.GetInt32(75, 3);
            set => RecordData.SetInt32(75, 3, value);
        }

        public char OriginatorStatusCode
        {
            get => RecordData.GetChar(78);
            set => RecordData.SetChar(78, value);
        }

        public string OriginatingDfiIdentification
        {
            get => RecordData.GetString(79, 8);
            set => RecordData.SetString(79, 8, value);
        }

        public int? BatchNumber
        {
            get => RecordData.GetInt32(87, 7);
            set => RecordData.SetInt32(87, 7, value);
        }

        public BatchHeaderRecord(ReadOnlySpan<char> record)
            : base(record)
        { }

        public BatchHeaderRecord(ReadOnlyMemory<char> record)
            : base(record)
        { }
    }
}
