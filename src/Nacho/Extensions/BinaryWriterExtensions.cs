﻿#if NUGET_SYSTEM_MEMORY
using System;
using System.IO;

// Not placing this in the .Extensions namespace in order to simplify the compiler flag
// detection in other files. (Removes the need to also conditionally include the namespace
// if not already included.)
namespace Nacho
{
    public static class BinaryWriterExtensions
    {
        public static void Write(this BinaryWriter writer, ReadOnlySpan<char> span)
        {
            writer.Write(span.ToArray());
        }

        public static void Write(this BinaryWriter writer, ReadOnlyMemory<char> memory)
        {
            writer.Write(memory.ToArray());
        }
    }
}
#endif