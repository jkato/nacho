using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nacho.Records;

namespace Nacho
{
    public interface IAch
    {
        /// <summary>
        /// Loads ACH records from the specified file.
        /// </summary>
        /// <param name="file">The ACH file to load.</param>
        /// <returns>A collection of <see cref="AchRecord"/> that were found in the file.</returns>
        AchRecord[] Load(FileInfo file);

        /// <summary>
        /// Load ACH records from the stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns>A collection of <see cref="AchRecord"/> that were found in the file.</returns>
        AchRecord[] Load(Stream stream);

        /// <summary>
        /// Load ACH records from the specified file.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns>A collection of <see cref="AchRecord"/> that were found in the file.</returns>
        AchRecord[] Load(string filePath);

        /// <summary>
        /// Writes a collection of records to a file.
        /// </summary>
        /// <param name="file">The file where records will be written.</param>
        /// <param name="records">The records to write.</param>
        /// <param name="lineEnding">The line ending for each record.</param>
        void Write(FileInfo file, IEnumerable<AchRecord> records, LineEnding lineEnding = LineEnding.CrLf);

        /// <summary>
        /// Writes a collection of records to a file.
        /// </summary>
        /// <param name="filePath">The file where records will be written.</param>
        /// <param name="records">The records to write.</param>
        /// <param name="lineEnding">The line ending for each record.</param>
        void Write(string filePath, IEnumerable<AchRecord> records, LineEnding lineEnding = LineEnding.CrLf);
    }

    public class Ach : IAch
    {
        public const int RECORD_LENGTH = 94;

        public static string[] CreditTransactionCodes { get; } =
        {
            "22", "32", "42", "52", 
        };

        public static string[] DebitTransactionCodes { get; } =
        {
            "27", "37", "47", "57",
        };

        /// <inheritdoc cref="IAch.Load(FileInfo)"/>
        public AchRecord[] Load(FileInfo file)
        {
            if (file == null || !file.Exists)
                return new AchRecord[] { };

            using var fs = file.OpenRead();
            return LoadAch(fs);
        }

        /// <inheritdoc cref="IAch.Load(Stream)"/>
        public AchRecord[] Load(Stream stream)
        {
            if (stream == null)
                return new AchRecord[] { };

            return LoadAch(stream);
        }

        /// <inheritdoc cref="IAch.Load(string)"/>
        public AchRecord[] Load(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath) || !File.Exists(filePath))
                return new AchRecord[] { };

            using var fs = File.Open(filePath, FileMode.Open, FileAccess.Read);
            return LoadAch(fs);
        }

        public void Write(Stream stream, IEnumerable<AchRecord> records, LineEnding lineEnding = LineEnding.CrLf)
        {
            if (stream == null)
                return;

            if (records == null)
                return;

            WriteAch(stream, records, lineEnding);
        }

        /// <inheritdoc cref="IAch.Write(FileInfo, IEnumerable{AchRecord}, LineEnding)"/>
        public void Write(FileInfo file, IEnumerable<AchRecord> records, LineEnding lineEnding = LineEnding.CrLf)
        {
            if (file == null || !file.Exists)
                return;

            using var fs = file.OpenWrite();
            WriteAch(fs, records, lineEnding);
        }

        /// <inheritdoc cref="IAch.Write(string, IEnumerable{AchRecord}, LineEnding)"/>
        public void Write(string filePath, IEnumerable<AchRecord> records, LineEnding lineEnding = LineEnding.CrLf)
        {
            if (string.IsNullOrWhiteSpace(filePath) || !File.Exists(filePath))
                return;

            using var fs = File.OpenWrite(filePath);
            WriteAch(fs, records, lineEnding);
        }

        private AchRecord[] LoadAch(Stream stream)
        {
            using var rd = new BinaryReader(stream, Encoding.ASCII);

            // File is too small, we can't read any records.
            if (stream.Length < RECORD_LENGTH)
                return new AchRecord[] { };

            if (!stream.CanSeek)
            {
                throw new InvalidOperationException(
                    "Cannot read ACH records from a stream that does not support Seek.");
            }

            // Move past what should be the first record so we can inspect
            // the line ending if any exists.
            stream.Seek(RECORD_LENGTH, SeekOrigin.Begin);
            var lineEndChar = rd.ReadChar();
            var skipChars = GetSkipCharCount(lineEndChar);

            // Move back to the beginning of the file
            stream.Seek(0, SeekOrigin.Begin);

            var segments = new List<Memory<char>>();
            while (rd.PeekChar() >= 0)
            {
                var buffer = new Memory<char>(rd.ReadChars(RECORD_LENGTH));
                segments.Add(buffer);

                stream.Seek(stream.Position + skipChars, SeekOrigin.Begin);
            }

            var opts = new ParallelOptions
            {
                MaxDegreeOfParallelism = Environment.ProcessorCount,
            };

            // Parse the records from the original set of data that we read in.
            var records = new ConcurrentDictionary<int, AchRecord>();
            Parallel.ForEach(segments, opts, (seg, state, idx) =>
            {
                var record = ParseRecord(seg);
                records.TryAdd((int)idx, record);
            });

            // Sort the records so they're in order in a collection
            var sortedRecords = records.OrderBy(kv => kv.Key)
                .Select(kv => kv.Value)
                .ToArray();

            // Re-parse detail records into their correct record formats based
            // on the ECC for the nearest batch header.
            var ecc = "";
            for (var i = 0; i < sortedRecords.Length; i++)
            {
                var record = sortedRecords[i];
                if (record is BatchHeaderRecord header)
                {
                    ecc = header.EntryClassCode?.ToUpper() ?? "";
                    continue;
                }

                if (record.RecordTypeCode == '6')
                {
                    sortedRecords[i] = ParseDetailRecord(ecc, record);
                }

                if (record.RecordTypeCode == '7')
                {
                    sortedRecords[i] = ParseAddendaRecord(ecc, record);
                }
            }

            return sortedRecords;
        }

        private void WriteAch(Stream stream, IEnumerable<AchRecord> records, LineEnding lineEnding = LineEnding.CrLf)
        {
            if (!stream.CanWrite)
            {
                throw new InvalidOperationException(
                    "Cannot write ACH records to a stream that does not support Write.");
            }

            using var wr = new BinaryWriter(stream);

            foreach (var rec in records)
            {
                wr.Write(rec.RecordData.Span);
                switch (lineEnding)
                {
                    case LineEnding.CrLf:
                        wr.Write('\r');
                        wr.Write('\n');
                        break;
                    case LineEnding.Lf:
                        wr.Write('\n');
                        break;
                }
            }

            stream.Flush();
        }

        private int GetSkipCharCount(char lineEnding) => lineEnding switch
        {
            '\n' => 1,
            '\r' => 2,
            _ => 0
        };

        private AchRecord ParseRecord(Memory<char> record) => record.Span[0] switch
        {
            '1' => new FileHeaderRecord(record),
            '9' => new FileControlRecord(record),
            '5' => new BatchHeaderRecord(record),
            '8' => new BatchControlRecord(record),
            _ => new AchRecord(record),
        };

        private AchRecord ParseDetailRecord(string ecc, AchRecord record) => ecc switch
        {
            "ARC" => new DetailRecordArc(record.RecordData),
            "BOC" => new DetailRecordBoc(record.RecordData),
            "CCD" => new DetailRecordCcd(record.RecordData),
            "CIE" => new DetailRecordCie(record.RecordData),
            "CTX" => new DetailRecordCtx(record.RecordData),
            "DNE" => new DetailRecordDne(record.RecordData),
            "ENR" => new DetailRecordEnr(record.RecordData),
            "MTE" => new DetailRecordMte(record.RecordData),
            "POP" => new DetailRecordPop(record.RecordData),
            "POS" => new DetailRecordPos(record.RecordData),
            "PPD" => new DetailRecordPpd(record.RecordData),
            "RCK" => new DetailRecordRck(record.RecordData),
            "SHR" => new DetailRecordShr(record.RecordData),
            "TEL" => new DetailRecordTel(record.RecordData),
            "TRC" => new DetailRecordTrc(record.RecordData),
            "TRX" => new DetailRecordTrx(record.RecordData),
            "WEB" => new DetailRecordWeb(record.RecordData),
            "XCK" => new DetailRecordXck(record.RecordData),
            _ => record,
        };

        private AchRecord ParseAddendaRecord(string ecc, AchRecord record) => ecc switch
        {
            "CCD" => new AddendaRecordCcd(record.RecordData),
            "CIE" => new AddendaRecordCie(record.RecordData),
            "CTX" => new AddendaRecordCtx(record.RecordData),
            "DNE" => new AddendaRecordDne(record.RecordData),
            "ENR" => new AddendaRecordEnr(record.RecordData),
            "MTE" => new AddendaRecordMte(record.RecordData),
            "POS" => new AddendaRecordPos(record.RecordData),
            "PPD" => new AddendaRecordPpd(record.RecordData),
            "SHR" => new AddendaRecordShr(record.RecordData),
            "TRX" => new AddendaRecordTrx(record.RecordData),
            "WEB" => new AddendaRecordWeb(record.RecordData),
            _ => record,
        };
    }
}
