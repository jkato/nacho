using System.IO;
using System.Linq;
using Xunit;
using Xunit.Abstractions;

namespace Nacho.Tests.Unit
{
    [Trait("Category", "Unit")]
    public class AchUnitTests
    {
        private ITestOutputHelper Output { get; }

        public AchUnitTests(ITestOutputHelper output)
        {
            Output = output;
        }

        [Fact]
        public void Load_ReturnsSetOfRecordsFromStream()
        {
            // Convert the file into a stream.
            using var stream = new MemoryStream(string.Join("", ExampleFile1).Select(c => (byte)c).ToArray());
            var sut = new Ach();

            var records = sut.Load(stream);

            Assert.NotNull(records);
            Assert.NotEmpty(records);
        }

        [Fact]
        public void Load_ReturnsEmptyRecordSetWhenStreamIsNull()
        {
            var sut = new Ach();

            var records = sut.Load((MemoryStream) null);

            Assert.NotNull(records);
            Assert.Empty(records);
        }

        [Fact]
        public void Load_ReturnsEmptyRecordSet_WhenFileDoesNotExist()
        {
            const string filePath = "TestFile.ach";

            var sut = new Ach();

            var records = sut.Load(filePath);
            Assert.NotNull(records);
            Assert.Empty(records);
        }

        [Fact]
        public void Load_ReturnsEmptyRecordSet_WhenFileInfoNull()
        {
            var sut = new Ach();

            var records = sut.Load((FileInfo) null);
            Assert.NotNull(records);
            Assert.Empty(records);
        }

        [Fact]
        public void Load_ReturnsEmptyRecordSet_WhenFileInfoPointsToMissingFile()
        {
            var file = new FileInfo("TestFile.ach");

            var sut = new Ach();

            var records = sut.Load(file);
            Assert.NotNull(records);
            Assert.Empty(records);
        }

        private static string[] ExampleFile1 { get; } =
        {
            "101 123123123 12312312319112020081094101EXAMPLE DESTINATION    EXAMPLE ORIGIN         00000001",
            "55555555555555555555555555555555555555555555555555CCD55555555555555555555555555555555555555555",
            "62212312312301234567890TEST  00000002001              CCD Test 1              1000000000000001",
            "705                                                                                00010000001",
            "62712312312301234567890TEST  00000002001              CCD Test 2              1000000000000001",
            "705                                                                                00010000001",
            "80000000040000000000000000000000000000000000                                   123123120000001",
            "900000000000000000000          000000000000000000000000                                       "
        };
    }
}
