param(
    [string] $ProjectName = 'Nacho'
)
# Based on StackOverflow Answer:
# https://stackoverflow.com/a/53197751
$RootDir  = [System.IO.Path]::GetFullPath("$PSScriptRoot\..")
$ProjFile = [System.IO.Path]::Combine($RootDir, "src", $ProjectName, "$ProjectName.csproj")

$CsProj        = [Xml] (Get-Content $ProjFile)
$PropertyGroup = $CsProj.Project.PropertyGroup

$Version = $null
if ($PropertyGroup -is [Array]) {
    $Version = [version] $CsProj.Project.PropertyGroup[0].Version
} else {
    $Version = [version] $CsProj.Project.PropertyGroup.Version
}

Write-Host "Version: $($Version.ToString(3))"
$env:NACHO_VERSION = $Version
