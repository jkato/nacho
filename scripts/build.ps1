param(
    [string] $Configuration = 'Release'
)

$RootDir  = [System.IO.Path]::GetFullPath("$PSScriptRoot\..")
$BuildDir = [System.IO.Path]::Combine($RootDir, "package")
$ProjFile = [System.IO.Path]::Combine($RootDir, "src", "Nacho", "Nacho.csproj")

If (Test-Path $BuildDir) {
    Remove-Item -Force -Recurse "$RootDir\package\*"
}

dotnet pack -c $Configuration -o $BuildDir $ProjFile